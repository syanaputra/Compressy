<?php
// Define Config
$config = array(
    'allowed_file_types' => array('jpeg', 'jpg', 'png'),
    'upload_folder' => 'uploads',
    'output_folder' => 'resized',
);
