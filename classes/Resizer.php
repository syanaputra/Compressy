<?php
// Load from vendor
include "vendor/autoload.php";

use \Eventviva\ImageResize;

// Create a class
// -----------------------
class Resizer {
    var $output_folder;
    var $upload_folder;
    static $FOLDER_PERMISSION = '644';

    function __construct($output_folder = "", $upload_folder = "") {
        $this->output_folder = $output_folder;
        $this->upload_folder = $upload_folder;
    }

    function performResize($filename, $options = array()) {
        $new_filename = $this->output_folder . substr($filename, strlen($this->upload_folder));

        // Create directory if it doesn't exist
        $folder = dirname($new_filename);
        if (!is_dir($folder)) {
            mkdir($folder, self::$FOLDER_PERMISSION, true);
        }

        // Start resizing
        $image = new ImageResize($filename);

        $max_width = isset($options['max_width']) && $options['max_width'] > 0 ? $options['max_width'] : null;
        $max_height = isset($options['max_height']) && $options['max_height'] > 0 ? $options['max_height'] : null;
        $image_quality = isset($options['image_quality']) && $options['image_quality'] > 0 ? $options['image_quality'] : 75;

        if($max_width && $max_height) {
            $image->resizeToBestFit($max_width, $max_height);
        }
        elseif($max_width) {
            $image->resizeToWidth($max_width);
        }
        elseif($max_height) {
            $image->resizeToHeight($max_height);
        }

        $image->quality_jpg = $image_quality;
        $image->quality_png = round($image_quality / 10);
        $image->save($new_filename);

        return "Succesfully resized <span>$new_filename</span>";
    }

    // This function scans the files folder recursively, and builds a large array
    // Source: http://tutorialzine.com/2014/09/cute-file-browser-jquery-ajax-php/
    function scan($dir, &$unique_id = 1) {
    	$files = array();

    	// Is there actually such a folder/file?
    	if(file_exists($dir)){

    		foreach(scandir($dir) as $f) {

    			if(!$f || $f[0] == '.') {
    				continue; // Ignore hidden files
    			}

    			if(is_dir($dir . DIRECTORY_SEPARATOR  . $f)) {

    				// The path is a folder

    				$files[] = array(
                        "id" => $unique_id++,
    					"name" => $f,
    					"type" => "folder",
    					"path" => $dir . DIRECTORY_SEPARATOR  . $f,
    					"items" => $this->scan($dir . DIRECTORY_SEPARATOR  . $f, $unique_id) // Recursively get the contents of the folder
    				);
    			}

    			else {

    				// It is a file
                    $type = 'file';
                    $path_parts = pathinfo($dir . DIRECTORY_SEPARATOR . $f);
                    if(in_array($path_parts['extension'], array('jpg', 'pdf', 'png'))) {
                        $type = $path_parts['extension'];
                    }

    				$files[] = array(
                        "id" => $unique_id++,
    					"name" => $f,
    					"type" => $type,
    					"path" => $dir . DIRECTORY_SEPARATOR  . $f,
    					"size" => filesize($dir . DIRECTORY_SEPARATOR  . $f) // Gets the size of this file
    				);
    			}
    		}

    	}

    	return $files;
    }

}
