module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        jshint: {
            dev: {
                src: ['js/*.js']
            }
        },

        autoprefixer: {
            options: {
                browsers: ['last 2 version', 'ie 8', 'ie 9'],
                map: true
            },
            multiple_files: {
                expand: true,
                flatten: true,
                src: 'css/*.css',
                dest: 'css/'
            }
        },

        less: {
             production: {
                 options: {
                     cleancss: true
                 },
                 files: {"css/main.css": "less/*.less"}
             }
         },

         watch: {
            js: {
                files: ['js/*.js','js/**/*.js'],
                tasks: [],
                options: {
                    spawn: false
                }
            },
            css: {
                files: ['less/*.less', 'less/**/*.less'],
                tasks: ['less','autoprefixer'],
                sourceComments: 'normal',
                options: {
                    livereload: true,
                    spawn: false
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-autoprefixer');

    // task setup
    grunt.registerTask('default', ['less','jshint']);
    grunt.registerTask('dev', ['watch']);
    grunt.registerTask('production', ['less','autoprefixer']);
};
