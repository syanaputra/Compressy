<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Compress and resize your jpg and png images using Compressy.">
    <meta name="author" content="Stephanus Yanaputra">

    <title>Compress Your Images | Compressy</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="templates/default/css/main.css" rel="stylesheet">
</head>

<body>

    <header class="header">
        <div class="container-fluid">
            <div class="row">

                <div class="col-xs-3">
                </div>

                <div class="col-xs-6">
                    <div class="logo">
                        <div class="logo__circle">
                            compressy
                        </div>
                    </div>
                </div>

                <div class="col-xs-3 text-right">
                    <a class="header__notification" href="#" data-display-notification><img src="templates/default/img/notification.svg" /></a>
                </div>

            </div>
        </div>

    </header>
