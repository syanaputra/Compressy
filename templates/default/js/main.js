$(function() {
    // A hack to serialize form into object
    // Source: http://stackoverflow.com/questions/17488660/difference-between-serialize-and-serializeobject-jquery
    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    $('.file-list__toggle').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var $this = $(this),
            $parent = $this.closest('li');
            $children = $parent.children('ul');

        $children.slideToggle("fast");

        return false;
    });

    $('[data-toggle-select-all]').on('change', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var $this = $(this),
            $parent = $this.closest('li');
            $children = $parent.find(':checkbox');

        $children.prop('checked', this.checked);
    });

    $('[data-compress]').on('submit', function(e) {
        e.preventDefault();

        var $this = $(this),
            $input = $('.compress-selected:checked'),
            $button = $this.find('button'),
            url = 'compress/',
            data = $this.serializeObject(),
            files = $input.map(function() {
                return $(this).val();
            }).get(),
            target = null,
            $log = $('.notification__log'),
            totalProgress = files.length,
            currentProgress = 0,
            percentageProgress = 0;

        // Loop to call AJAX
        if(files.length > 0 && !$button.hasClass('disabled')) {
            // Clear the log & progress bar
            clearNotification();
            clearProgressBar();
            showNotificationBar();

            $button.addClass('disabled').html('Compressing');

            // Create delay to let the animation finish
            setTimeout(function() {
                // Loop
                for(var i=0; i<files.length; i++) {
                    // Update data
                    data.target = files[i];
                    console.log(data);

                    // Send AJAX request
                    $.post(url, data)
                    .error(function(response) {
                        console.log(response);
                        createNotification(response);
                    })
                    .done(function(response) {
                        console.log(response);
                        createNotification(response);
                    })
                    .always(function(response) {
                        currentProgress++;
                        percentageProgress = currentProgress / totalProgress * 100;
                        updateProgressBar(percentageProgress);

                        if(percentageProgress >= 100) {
                            $button.removeClass('disabled').html('Compress');
                        }
                    });
                }
            }, 300);

        }

        return false;
    });

    $('[data-display-notification]').on('click', function(e) {
        e.preventDefault();

        showNotificationBar();
    });

    $('[data-close-notification]').on('click', function(e) {
        e.preventDefault();

        hideNotificationBar();
    });

    var showNotificationBar = function() {
        $('.notification').addClass('notification--active');
    }

    var hideNotificationBar = function() {
        $('.notification').removeClass('notification--active');
    }

    var clearProgressBar = function(progress) {
        $('.notification__progress__bar').css('width', '0%');
    }

    var updateProgressBar = function(progress) {
        $('.notification__progress__bar').css('width', progress + '%');
    }

    var clearNotification = function() {
        $('.notification__logs').html('');
    }

    var createNotification = function(response) {
        var response_css = response.status == 1 ? 'success' : 'error',
            message = response.message ? response.message : 'Unknown message.',
            $notification = $('<p class="notification__log notification__log--'+response_css+'">'+message+'</p>');

        $('.notification__logs').append($notification);
        setTimeout(function() {
            $notification.addClass('notification__log--active');
        }, 100);
    }
});
