<?php include("header.php"); ?>

<form action="compress/" data-compress>
    <div class="container">
        <div class="window">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <h2 class="heading heading--lv2 heading--mini-spacing">Settings</h2>
                    <div class="text-box">
                        <p>Adjust your compression preferences</p>
                    </div>

                    <div class="form-row">
                        <h3 class="heading heading--lv4">Compress the following types</h3>
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="checkbox-special">
                                    <input type="checkbox" value="jpg" name="compress_type[]" id="compress_type_jpg" checked="checked" />
                                    <label for="compress_type_jpg">JPG</label>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="checkbox-special">
                                    <input type="checkbox" value="png" name="compress_type[]" id="compress_type_png" checked="checked" />
                                    <label for="compress_type_png">PNG</label>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="checkbox-special">
                                    <input type="checkbox" value="pdf" name="compress_type[]" id="compress_type_pdf" />
                                    <label for="compress_type_pdf">PDF</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <h3 class="heading heading--lv4">Image compression settings</h3>
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label for="image_quality">Image Quality (0-100)</label>
                                    <input type="number" class="form-control" name="image_quality" id="image_quality" value="75" min="0" max="100" required />
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label for="max_width">Max Width (in px)</label>
                                    <input type="number" class="form-control" name="max_width" id="max_width" min="0" />
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label for="max_height">Max Height (in px)</label>
                                    <input type="number" class="form-control" name="max_height" id="max_height" min="0" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6">
                    <h2 class="heading heading--lv2 heading--mini-spacing">Compression Target</h2>
                    <div class="text-box">
                        <p>Select files and folders to compress</p>
                    </div>

                    <div class="file-browser">
                        <div class="file-browser__header">

                        </div>

                        <div class="file-browser__body">
                            <ul class="file-list">
                            <?php
                                function recurseFileDisplay($files, $level = 0) {
                                    foreach($files as $key => $file) :
                                        $icon_url = "design-48px-outline_paper-design.svg";
                                        switch(strtolower($file['type'])) {
                                            case 'folder':
                                                $icon_url = 'files-48px-outline_folder-15.svg';
                                            break;

                                            case 'pdf':
                                                $icon_url = 'files-48px-outline-3_pdf.svg';
                                            break;

                                            case 'jpg': case 'jpeg':
                                                $icon_url = 'files-48px-outline-2_jpg-jpeg.svg';
                                            break;

                                            case 'png':
                                                $icon_url = 'files-48px-outline-3_png.svg';
                                            break;
                                        }
                                         ?>
                                    <li id="file-<?php echo $file['id']?>">
                                        <div class="file-list__row">
                                            <div class="file-list__col file-list__col--checkbox">
                                                <div class="checkbox-special">
                                                    <input type="checkbox" value="<?php echo $file['path']; ?>" <?php if ($file['type'] != 'folder') : ?>class="compress-selected"<?php endif; ?> id="compress_selected_<?php echo $file['id']?>" data-toggle-select-all />
                                                    <label for="compress_selected_<?php echo $file['id']?>">&nbsp;</label>
                                                </div>
                                            </div>

                                            <div class="file-list__col file-list__col--icon">
                                                <img src="templates/default/img/<?php echo $icon_url; ?>" />
                                            </div>

                                            <div class="file-list__col file-list__col--name file-list__col--body-name">
                                                <div class="<?php if(isset($file['items']) && sizeof($file['items'])) : ?>file-list__toggle<?php endif; ?>" style="padding-left: <?php echo $level * 15?>px">
                                                    <?php echo $file['name']; ?>
                                                </div>
                                            </div>

                                            <div class="file-list__col file-list__col--type file-list__col--body-type">
                                                <?php echo $file['type']; ?>
                                            </div>
                                        </div>
                                        <?php if(isset($file['items']) && sizeof($file['items'])) : ?>
                                        <ul style="display: none;">
                                            <?php recurseFileDisplay($file['items'], $level+1); ?>
                                        </ul>
                                        <?php endif; ?>
                                    </li>
                                    <?php endforeach;
                                }

                                // Run the recursive function
                                recurseFileDisplay($files);
                            ?>
                            </ul>
                        </div>

                        <div class="file-browser__footer">

                        </div>
                    </div>
                </div>
            </div>

            <div class="window__action">
                <button type="submit" class="btn btn-primary">Compress</button>
            </div>
        </div>
    </div>
</form>

<div class="notification">
    <div class="notification__close">
        <button class="notification__close-button" data-close-notification>close</button>
    </div>

    <div class="notification__progress">
        <div class="notification__progress__bar" style="width: 0%;"></div>
    </div>

    <h3 class="heading heading--lv3">Notification</h3>
    <div class="notification__logs">

    </div>
</div>

<?php include("footer.php"); ?>
