<?php
require("vendor/autoload.php");
require("settings.php");

spl_autoload_register(function ($classname) {
    require ("./classes/" . $classname . ".php");
});

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Eventviva\ImageResize;

// Create app
$app = new \Slim\App([
    "config" => $config,
]);

// Get container
$container = $app->getContainer();

// Register component on container
$container['view'] = function ($container) {
    return new \Slim\Views\PhpRenderer('./templates/default');
};

// Index
$app->get('/', function (Request $request, Response $response) {
    // Get Data
    $dir = $this->config['upload_folder'];

    $resizer = new Resizer();
    $scanResponse = $resizer->scan($dir);
    $data['files'] = $scanResponse;
    $data['config'] = $this->config;

    // View
    return $this->view->render($response, '/main.php', $data);
});

// Compress Image
$app->post('/compress/', function (Request $request, Response $response) {
    $data = $request->getParsedBody();

    // Start the magic
    // -----------------------
    $target = isset($data['target']) ? filter_var($data['target'], FILTER_SANITIZE_STRING) : null;
    $output = array(
        'status' => 0,
        'message' => 'Something went wrong',
    );
    $messages = array();

    if($target) {
        // Get allowed file types
        $allowed_file_types = $this->config['allowed_file_types'];

        if(isset($data['compress_type']) && $data['compress_type'] > 0) {
            $allowed_file_types = $data['compress_type'];
        }

        // Get target info
        $path_parts = pathinfo($target);

        // Only perform on allowed_types
        if (!in_array(strtolower($path_parts['extension']), $allowed_file_types)) {
            $output['message'] = "Unable to process <span>{$target}</span>. File type <code>{$path_parts['extension']}</code> is not allowed.";
        }
        else {
            // Image Resize Settings
            // ------------------------------
            $options = array();

            if(isset($data['max_width']) && $data['max_width'] > 0) {
                $options['max_width'] = $data['max_width'];
            }
            if(isset($data['max_height']) && $data['max_height'] > 0) {
                $options['max_height'] = $data['max_height'];
            }
            if(isset($data['image_quality']) && $data['image_quality'] > 0) {
                $options['image_quality'] = $data['image_quality'];
            }

            $output_folder = $this->config['output_folder'];
            $upload_folder = $this->config['upload_folder'];
            
            // Start resizing
            $resizer = new Resizer($output_folder, $upload_folder);
            $messages[] = $resizer->performResize($target, $options);
            
            $output['status'] = 1;
            $output['message'] = implode("<br />", $messages);
        }
    }

    return $response->withJson($output);
});

// Run the app
$app->run();
