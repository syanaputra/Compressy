<?php
// Load from vendor
include "vendor/autoload.php";
include "settings.php";

use \Eventviva\ImageResize;

$target = isset($_GET['target']) ? $_GET['target'] : null;

if($target) {

    // Image Resize Settings
    // ------------------------------
    $max_width = 1600;
    $max_height = 1300;

    $resizer = new Resizer($config['output_folder'], $config['upload_folder']);

    if(!is_dir($target)) {
        // Get directory and check if it exists
        $folder = dirname($target);
        if (!is_dir($folder)) {
            echo "<p>Creating folder <code>$folder</code></p>";
            mkdir($folder);
        }

        // Perform image resize
        $resizer->performResize($filename);
    }
    else {
        // Loop through folder
        // ------------------------------
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($target));
        foreach ($files as $name => $file) {

            if ($file->isDir()) {
                if ($file->getFileName() == '.') {
                    $filename = $file->getPathName();
                    $new_filename = $output_folder . "/" . substr($filename, strlen($upload_folder));

                    // Create directory if it doesn't exist
                    $folder = substr($new_filename, 0, -1);
                    if (!is_dir($folder)) {
                        echo "<p>Creating folder <code>$folder</code></p>";
                        mkdir($folder);
                    }
                }

                continue;
            }

            if (in_array($file->getExtension(), $config['allowed_file_types'])) {
                $filename = $file->getPathName();
                $resizer->performResize($filename, $new_filename);
            }
        }
    }
}

class Resizer {
    var $output_folder;
    var $upload_folder;

    function __construct($output_folder = "", $upload_folder = "") {
        $this->output_folder = $output_folder;
        $this->upload_folder = $upload_folder;
    }

    function performResize($filename) {
        $new_filename = $this->output_folder . "/" . substr($filename, strlen($this->upload_folder));

        $image = new ImageResize($filename);
        //$image->resizeToBestFit($max_width, $max_height);
        $image->quality_jpg = 70;
        $image->save($new_filename);

        echo "<p>Resizing <code>$new_filename</code></p>";
    }
}
